public class Magasin{

    private String nom;
    private boolean ouvertLundi;
    private boolean ouvertDimanche;

    public Magasin(String nom,boolean lundi,boolean dimanche){
        this.nom = nom;
        this.ouvertLundi = lundi;
        this.ouvertDimanche = dimanche;
    }

    public String getNom(){
        return this.nom;
    }

    public boolean ouvertLundi(){
        return this.ouvertLundi;
    }

    public boolean ouvertDimanche(){
        return this.ouvertDimanche;
    }

    @Override
    
    public String toString(){
        return "Magasin: "+this.getNom()+" ouvert lundi: "+this.ouvertLundi()+" ouvert dimanche: "+this.ouvertDimanche();
    }
}