import java.util.ArrayList;
import java.util.List;

public class ExecutableMagasins{
    public static void main(String[] args){
        
        Magasin fleurus = new Magasin("Fleurus", true, false);
        
        Ville trainou = new Ville("Trainou");
        
        trainou.ajouteMagasin("BeauMagasin", true, true);
        trainou.ajouteMagasin("Venir", false,false);
        trainou.ajouteMagasin("Magnifique", false, true);
        trainou.ajouteMagasin("Beauchamp", true, false);
        
        System.out.println(fleurus.toString());

        System.out.println("------");

        System.out.println(trainou.toString());
    
        System.out.println("------");

        for (Magasin magasin : trainou.ouvertsLeLundi())
            System.out.println(magasin.getNom()+" est ouvert le lundi");

    }
}